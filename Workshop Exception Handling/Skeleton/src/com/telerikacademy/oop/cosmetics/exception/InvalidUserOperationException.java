package com.telerikacademy.oop.cosmetics.exception;

public class InvalidUserOperationException extends RuntimeException{
    public InvalidUserOperationException(String message) {
        super(message);
    }
}
