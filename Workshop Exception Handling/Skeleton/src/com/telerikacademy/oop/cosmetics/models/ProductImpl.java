package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.core.contracts.Product;
import com.telerikacademy.oop.cosmetics.exception.InvalidUserInputException;

public class
ProductImpl implements Product {
    public static final int MIN_LENGTH_NAME = 3;
    public static final int MAX_LENGTH_NAME = 10;
    public static final int MIN_LENGTH_BRAND = 2;
    public static final int MAX_LENGTH_BRAND = 10;
    public static final int MIN_PRICE = 0;
    private String name;
    private String brand;
    private double price;
    private final GenderType gender;

    public ProductImpl(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name.length() < MIN_LENGTH_NAME || name.length() > MAX_LENGTH_NAME) {
            throw new InvalidUserInputException(String.format("Product name should be between %d and %d symbols.",
                    MIN_LENGTH_NAME, MAX_LENGTH_NAME));
        }
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        if (brand.length() < MIN_LENGTH_BRAND || brand.length() > MAX_LENGTH_BRAND) {
            throw new InvalidUserInputException(String.format("Product brand should be between 2 and 10 symbols.",
                    MIN_LENGTH_BRAND, MAX_LENGTH_BRAND));
        }
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        if (price < MIN_PRICE) {
            throw new InvalidUserInputException("Price can't be negative.");
        }
        this.price = price;
    }

    public GenderType getGender() {
        return gender;
    }

    @Override
    public String print() {
        return String.format(
                "#%s %s%n" +
                        " #Price: $%.2f%n" +
                        " #Gender: %s%n",
                name,
                brand,
                price,
                gender);
    }
}
