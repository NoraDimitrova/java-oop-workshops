package com.telerikacademy.oop.cosmetics.core.contracts;

import java.util.List;

public interface CommandParser {
    String parseCommand(String commandLine);

    List<String> parseParameters(String commandLine);
}
