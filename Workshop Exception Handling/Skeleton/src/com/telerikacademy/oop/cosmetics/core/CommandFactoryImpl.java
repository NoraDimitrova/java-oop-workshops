package com.telerikacademy.oop.cosmetics.core;

import com.telerikacademy.oop.cosmetics.commands.*;
import com.telerikacademy.oop.cosmetics.commands.CommandType;
import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CommandFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exception.InvalidUserInputException;


public class CommandFactoryImpl implements CommandFactory {
    private static final String INVALID_COMMAND = "Command %s is not supported.";

    @Override
    public Command createCommand(String commandTypeValue, ProductFactory productFactory, ProductRepository productRepository) {

        CommandType commandType = validateCommand(commandTypeValue);

        switch (commandType) {
            case CREATECATEGORY:
                return new CreateCategory(productRepository, productFactory);
            case CREATEPRODUCT:
                return new CreateProduct(productRepository, productFactory);
            case ADDPRODUCTTOCATEGORY:
                return new AddProductToCategory(productRepository);
            case SHOWCATEGORY:
                return new ShowCategory(productRepository);
            default:
                throw new UnsupportedOperationException(String.format(INVALID_COMMAND, commandTypeValue));
        }
    }

    private CommandType validateCommand(String command){
        try{
            return CommandType.valueOf(command.toUpperCase());
        } catch (IllegalArgumentException e){
            throw new InvalidUserInputException(String.format(INVALID_COMMAND, command));
        }
    }
}


