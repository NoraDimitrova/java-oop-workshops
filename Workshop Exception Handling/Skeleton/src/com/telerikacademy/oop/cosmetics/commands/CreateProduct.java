package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.Product;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.models.GenderType;

import java.util.List;

public class CreateProduct implements Command {
    private static final String PRODUCT_CREATED = "Product with name %s was created!";
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 4;

    private final ProductRepository productRepository;
    private final ProductFactory productFactory;
    private String result;

    public CreateProduct(ProductRepository productRepository, ProductFactory productFactory) {
        this.productRepository = productRepository;
        this.productFactory = productFactory;
    }

    private String name;
    private String brand;
    private double price;
    private GenderType gender;

    @Override
    public void execute(List<String> parameters) {
        validateParameters(parameters);
        parseParameters(parameters);
        result = createProduct(name, brand, price, gender);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String createProduct(String name, String brand, double price, GenderType gender) {
        if (productRepository.getProducts().containsKey(name)) {
            throw new IllegalArgumentException(String.format("Product %s already exist.", name));
        }

        Product product = productFactory.createProduct(name, brand, price, gender);
        productRepository.getProducts().put(name, product);

        return String.format(PRODUCT_CREATED, name);
    }

    private void validateParameters(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS)
            throw new IllegalArgumentException(String.format("CreateProduct command expects %d parameters.",
                    EXPECTED_NUMBER_OF_ARGUMENTS));
    }

    private void parseParameters(List<String> parameters) {
        try {
            name = parameters.get(0);
            brand = parameters.get(1);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format("Failed to parse parameters."));
        }

        try{
            price = Double.parseDouble(parameters.get(2));
        } catch (Exception e){
            throw new IllegalArgumentException(String.format("Third parameter should be price (real number)."));
        }

        try{
            gender = GenderType.valueOf(parameters.get(3).toUpperCase());
        } catch (Exception e){
            throw new IllegalArgumentException(String.format("Forth parameter should be one of Men, Women or Unisex."));
        }

    }


}
