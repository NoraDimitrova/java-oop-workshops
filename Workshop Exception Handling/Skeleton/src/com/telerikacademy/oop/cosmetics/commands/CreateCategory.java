package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.models.GenderType;

import java.util.List;

public class CreateCategory implements Command {
    private static final String CATEGORY_CREATED = "Category with name %s was created!";
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private final ProductRepository productRepository;
    private final ProductFactory productFactory;
    private String result;

    public CreateCategory(ProductRepository productRepository, ProductFactory productFactory) {
        this.productRepository = productRepository;
        this.productFactory = productFactory;
    }

    private String categoryName;

    @Override
    public void execute(List<String> parameters) {
        validateParameters(parameters);
        parseParameters(parameters);
        result = createCategory(categoryName);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String createCategory(String categoryName) {

        if (productRepository.getCategories().containsKey(categoryName)) {
            throw new IllegalArgumentException(String.format("Category %s already exist.", categoryName));
        }
        Category category = productFactory.createCategory(categoryName);
        productRepository.getCategories().put(categoryName, category);

        return String.format(CATEGORY_CREATED, categoryName);
    }

    private void validateParameters(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS)
            throw new IllegalArgumentException(String.format("CreateCategory command expects %d parameters.",
                    EXPECTED_NUMBER_OF_ARGUMENTS));
    }

    private void parseParameters(List<String> parameters) {
        try {
            categoryName = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format("Failed to parse CreateCategory command parameters."));
        }
    }

}
