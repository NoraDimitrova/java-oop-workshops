package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.core.contracts.Product;
import com.telerikacademy.oop.cosmetics.exception.InvalidUserOperationException;
import com.telerikacademy.oop.cosmetics.models.GenderType;

import java.util.FormatFlagsConversionMismatchException;
import java.util.List;
import java.util.MissingFormatArgumentException;
import java.util.logging.Formatter;

public class AddProductToCategory implements Command {
    private static final String PRODUCT_ADDED_TO_CATEGORY = "Product %s added to category %s!";
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private final ProductRepository productRepository;
    private String result;
    String categoryNameToAdd;
    String productNameToAdd;

    public AddProductToCategory(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void execute(List<String> parameters) {

        validateParameters(parameters);
        parseParameters(parameters);
        String categoryNameToAdd = parameters.get(0);
        String productNameToAdd = parameters.get(1);
        result = addProductToCategory(categoryNameToAdd, productNameToAdd);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String addProductToCategory(String categoryName, String productName) {

        validateProductAndCategoryExist(categoryName, productName);

        Category category = productRepository.getCategories().get(categoryName);
        Product product = productRepository.getProducts().get(productName);
        category.addProduct(product);
        return String.format(PRODUCT_ADDED_TO_CATEGORY, productName, categoryName);
    }

    private void validateProductAndCategoryExist(String categoryName, String productName) {
        if (!productRepository.getCategories().containsKey(categoryName)) {
            throw new IllegalArgumentException(String.format("Category %s does not exist.", categoryName));
        }

        if (!productRepository.getProducts().containsKey(productName)) {
            throw new IllegalArgumentException(String.format("Product %s does not exist.", productName));
        }
    }

    private void validateParameters(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS)
            throw new IllegalArgumentException(String.format("AddProduct command expects %d parameters.",
                    EXPECTED_NUMBER_OF_ARGUMENTS));
    }

    private void parseParameters(List<String> parameters) {
        try {
            categoryNameToAdd = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format("CategoryName must be in String format"));
        }

        try {
            productNameToAdd = parameters.get(1);
        } catch (Exception e) {
            throw new InvalidUserOperationException("ProductName must be in String format");
        }




    }

}
