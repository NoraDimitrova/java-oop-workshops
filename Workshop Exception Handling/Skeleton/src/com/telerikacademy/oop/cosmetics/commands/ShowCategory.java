package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.Product;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.exception.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;

import java.util.List;

public class ShowCategory implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private final ProductRepository productRepository;
    private String result;

    public ShowCategory(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void execute(List<String> parameters) {
        validateParameters(parameters);

        String categoryName = parameters.get(0);

        result = showCategory(categoryName);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String showCategory(String categoryName) {

        if (!productRepository.getCategories().containsKey(categoryName)) {
            return String.format("Category %s does not exist.", categoryName);
        }

        Category category = productRepository.getCategories().get(categoryName);

        return category.print();

    }

    private void validateParameters(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS)
            throw new InvalidUserInputException(String.format("ShowCategory command expects %d parameters.",
                    EXPECTED_NUMBER_OF_ARGUMENTS));
    }
}
