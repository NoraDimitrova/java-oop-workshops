package com.telerikacademy.oop.cosmetics.tests.core;


import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ProductRepositoryImplTests {
    private List<Product> products;
    private List<Category> categories;
    private ProductRepositoryImpl productRepository;

    @BeforeEach
    public void beforeSetUp() {
        this.productRepository = new ProductRepositoryImpl();
        this.products = new ArrayList<>();
        this.categories = new ArrayList<>();

    }

    @Test
    public void constructor_should_initializeAllCollections() {
        Assertions.assertTrue(productRepository.getCategories().size() == 0);
        Assertions.assertTrue(productRepository.getProducts().size() == 0);
    }

    @Test
    public void getCategories_should_returnCopyOfCollection() {
        this.categories = new ArrayList<>();
        Assertions.assertEquals(categories, productRepository.getCategories());

        //Arrange, Act
        productRepository.createCategory("Shampoo");
        productRepository.createCategory("Cream");
        //Assert
        Assertions.assertTrue(productRepository.getCategories().size() == 2);
    }

    @Test
    public void getProducts_should_returnCopyOfCollection() {
        this.products = new ArrayList<>();
        Assertions.assertEquals(true, products.isEmpty());

        // Arrange, Act
        productRepository.createProduct("Shampoo", "Lorella", 12, GenderType.WOMEN);
        productRepository.createProduct("Shoes", "Puma", 150, GenderType.MEN);
        //Assert
        Assertions.assertTrue(productRepository.getProducts().size() == 2);
    }

    @Test
    public void categoryExists_should_returnTrue_whenCategoryExists() {
        categories = new ArrayList<>();
        Category category = new CategoryImpl("Men");
        categories.add(category);
        Category category1 = new CategoryImpl("Men");
        categories.add(category1);
        Assertions.assertTrue(true, "Men");
        // Arrange, Act
        productRepository.createCategory("test");
        // Assert
        Assertions.assertTrue(productRepository.categoryExist("test"));
    }

    @Test
    public void productExists_should_returnTrue_whenProductExists() {
        // Arrange, Act
        productRepository.createProduct("Ball", "Nike", 12, GenderType.UNISEX);
        // Assert
        Assertions.assertTrue(productRepository.productExist("Ball"));
    }

    @Test
    public void createProduct_should_createSuccessfully_whenArgumentsAreValid() {
        //Arrange, Act
        productRepository.createProduct("Ball", "Nike", 12, GenderType.UNISEX);
        // Assert
        Assertions.assertEquals(true, productRepository.productExist("Ball"));

    }

    @Test
    public void createCategory_should_createSuccessfully_whenArgumentsAreValid() {
        // Arrange, Act
        productRepository.createCategory("test");
        // Assert
        Assertions.assertTrue(productRepository.getCategories().get(0).getName().equalsIgnoreCase("test"));

    }

    @Test
    public void findCategoryByName_should_returnCategory_ifExists() {
        //Arrange, Act
        productRepository.createProduct("Ball", "Nike", 12, GenderType.UNISEX);
        // Assert
        Assertions.assertEquals("Ball", productRepository.getProducts().get(0).getName());
    }

    @Test
    public void findCategoryByName_should_throwException_ifDoesNotExist() {
        // Arrange, Act
        productRepository.createCategory("test");
        // Assert
        Assertions.assertEquals("test", productRepository.getCategories().get(0).getName());
    }

    @Test
    public void findProductByName_should_returnCategory_ifExists() {

    }

    @Test
    public void findProductByName_should_throwException_ifDoesNotExist() {
        ProductRepository productRepository = new ProductRepositoryImpl();
        Assertions.assertThrows(InvalidUserInputException.class, () -> productRepository.findCategoryByName("Orange"));
    }


}
