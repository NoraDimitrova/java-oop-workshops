package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.CreateProductCommand;
import com.telerikacademy.oop.cosmetics.commands.ShowCategoryCommand;
import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowCategoryCommandTests {
    private ProductRepositoryImpl productRepository;
    private List<String> parameters;
    private ShowCategoryCommand showCategoryCommand;

    @BeforeEach
    public void beforeSetUp() {
        this.productRepository = new ProductRepositoryImpl();
        this.parameters = new ArrayList<>();
        this.showCategoryCommand = new ShowCategoryCommand(productRepository);
    }


    @Test
    public void execute_should_throwException_when_missingParameters() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> showCategoryCommand.execute(parameters));
    }

    @Test
    public void execute_should_ProductCommandToRepository_when_validParameters() {
        ProductRepositoryImpl productRepository = new ProductRepositoryImpl();
        ShowCategoryCommand showCategoryCommand = new ShowCategoryCommand(productRepository);
        parameters.add("Apple");
        Assertions.assertEquals("Apple", showCategoryCommand.execute(parameters));
    }


}
