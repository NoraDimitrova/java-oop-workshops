package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.AddProductToCategoryCommand;
import com.telerikacademy.oop.cosmetics.commands.CreateProductCommand;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class AddProductToCategoryCommandTests {
    private ProductRepository productRepository;
    private AddProductToCategoryCommand addProductToCategoryCommand;
    private List<String> parameters;
    private List<Category> categories;


    @BeforeEach
    public void AddProductToCategoryCommand() {
        this.productRepository = new ProductRepositoryImpl();
        this.addProductToCategoryCommand = new AddProductToCategoryCommand(productRepository);
        this.parameters = new ArrayList<>();
        this.categories=new ArrayList<>();
    }

    @Test
    public void execute_should_throwException_when_missingParameters() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> addProductToCategoryCommand.execute(parameters));
    }

    @Test
    public void execute_should_addNewProductCommandToRepository_when_validParameters() {
        ProductRepository productRepository=new ProductRepositoryImpl();
        AddProductToCategoryCommand addProductToCategoryCommand=new AddProductToCategoryCommand(productRepository);
        parameters.add("Cream");
        parameters.add("Shampoo");
        Assertions.assertEquals("2", addProductToCategoryCommand.execute(parameters));
    }


}
