package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.CreateCategoryCommand;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.DuplicateEntityException;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateCategoryCommandTests {

    private ProductRepository productRepository;
    private List<String> parameters;
    private CreateCategoryCommand createCategoryCommand;


    @BeforeEach
    public void beforeSetUp() {
        this.productRepository = new ProductRepositoryImpl();
        this.parameters = new ArrayList<>();
        this.createCategoryCommand = new CreateCategoryCommand(productRepository);
    }

    @Test
    public void execute_should_addNewCategoryToRepository_when_validParameters() {
        // Act
        parameters.add("Shampoo");
        parameters.add("Cream");
        parameters.add("Soap");
        String executeResult;
        executeResult = createCategoryCommand.execute(parameters);
        // Assert
        Assertions.assertTrue(executeResult != null);
    }

    @Test
    public void execute_should_throwException_when_missingParameters() {
        Assertions.assertThrows(InvalidUserInputException.class, ()->createCategoryCommand.execute(parameters));
    }

    @Test
    public void execute_should_throwException_when_duplicateCategoryName() {
        parameters.add("Soap");
        CreateCategoryCommand createCategoryCommandTwo=new CreateCategoryCommand(productRepository);
        createCategoryCommand.execute(parameters);
        Assertions.assertThrows(DuplicateEntityException.class, ()->createCategoryCommandTwo.execute(parameters));
    }

}
