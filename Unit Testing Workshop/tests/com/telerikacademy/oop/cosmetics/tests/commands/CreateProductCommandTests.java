package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.CreateProductCommand;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.exceptions.DuplicateEntityException;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateProductCommandTests {
    private ProductRepositoryImpl productRepository;
    private List<String> parameters;
    private CreateProductCommand createProductCommand;


    @BeforeEach
    public void beforeSetUp() {
        this.productRepository = new ProductRepositoryImpl();
        this.parameters = new ArrayList<>();
        this.createProductCommand = new CreateProductCommand(productRepository);

    }

    @Test
    public void execute_should_throwException_when_missingParameters() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> createProductCommand.execute(parameters));
    }

    @Test
    public void execute_should_addNewProductCommandToRepository_when_validParameters() {
        ProductRepositoryImpl productRepository = new ProductRepositoryImpl();
        CreateProductCommand createProductCommand = new CreateProductCommand(productRepository);
        parameters.add("Orange");
        parameters.add("Cream");
        parameters.add("10");
        parameters.add("Men");
        Assertions.assertEquals("Product with name Orange was created!", createProductCommand.execute(parameters));
    }

    @Test
    public void execute_should_Throw_Exception_when_GenderType_Not_Valid() {
        parameters.add("Orange");
        parameters.add("Cream");
        parameters.add("10");
        parameters.add("Girl");
        Assertions.assertThrows(InvalidUserInputException.class, () -> createProductCommand.execute(parameters));
    }


    @Test
    public void execute_should_Throw_Exception_when_Price_Not_Valid() {
        parameters.add("Orange");
        parameters.add("Cream");
        parameters.add("INVALID_PRICE_MESSAGE");
        parameters.add("Women");
        Assertions.assertThrows(InvalidUserInputException.class, () -> createProductCommand.execute(parameters));
    }


    @Test
    public void execute_should_throwException_when_duplicateCategoryName() {
        parameters.add("Orange");
        parameters.add("Cream");
        parameters.add("10");
        parameters.add("Men");
        CreateProductCommand createProductCommandSecond = new CreateProductCommand(productRepository);
        createProductCommandSecond.execute(parameters);
        Assertions.assertThrows(DuplicateEntityException.class, () -> createProductCommandSecond.execute(parameters));
    }


}


