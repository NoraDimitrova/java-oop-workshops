package com.telerikacademy.oop.cosmetics.tests.models;

import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ProductImplTests {


    @BeforeEach
    public void setUp() {
        Product product1 = new ProductImpl("Soap", "Eucerin", 7.50, GenderType.WOMEN);
    }

    @Test
    public void test_constructor_should_createProduct_when_argumentsAreValid() {
        Product product1 = new ProductImpl("Soap", "Eucerin", 7.50, GenderType.WOMEN);
        Assertions.assertTrue(product1 instanceof ProductImpl);
    }

    @Test
    public void test_constructor_should_throwException_when_nameLongerThanExpected() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> new ProductImpl("Soap Blue Sky", "Golden", 7.50, GenderType.WOMEN));
    }

    @Test
    public void test_constructor_should_throwException_when_nameShorterThanExpected() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> new ProductImpl("Sy", "Golden", 7.50, GenderType.MEN));
    }

    @Test
    public void test_constructor_should_throwException_when_brandShorterThanExpected() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> new ProductImpl("Sky", "G", 7.50, GenderType.WOMEN));
    }

    @Test
    public void test_constructor_should_throwException_when_priceBelowThanZero() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> new ProductImpl("Sky", "Golden", -1.50, GenderType.WOMEN));
    }

    @Test
    public void test_getName() {
        Product product1 = new ProductImpl("Soap", "Eucerin", 7.50, GenderType.WOMEN);
        Assertions.assertEquals("Soap", product1.getName());
    }

    @Test
    public void test_getBrand() {
        Product product1 = new ProductImpl("Soap", "Eucerin", 7.50, GenderType.UNISEX);
        Assertions.assertEquals("Eucerin", product1.getBrand());
    }

    @Test
    public void test_getPrice() {
        Product product1 = new ProductImpl("Soap", "Eucerin", 7.50, GenderType.WOMEN);
        Assertions.assertEquals(7.50, product1.getPrice());
    }

    @Test
    public void test_getGenderType() {
        Product product1 = new ProductImpl("Soap", "Eucerin", 7.50, GenderType.WOMEN);
        Assertions.assertEquals("Women", product1.getGender().toString());
    }

    @Test
    public void test_Print_Method() {

        Product product1 = new ProductImpl("Soap", "Eucerin", 7.50, GenderType.MEN);
        Assertions.assertEquals("#Soap Eucerin\n" +
                " #Price: $7,50\n" +
                " #Gender: Men\n", product1.print());
    }

}
