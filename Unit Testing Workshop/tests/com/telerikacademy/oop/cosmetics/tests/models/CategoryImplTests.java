package com.telerikacademy.oop.cosmetics.tests.models;

import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;



public class CategoryImplTests {

    @Test
    public void constructor_should_createCategory_when_argumentsAreValid() {
        // Arrange, Act
        CategoryImpl shampoo = new CategoryImpl("Shampoo");
        // Assert
        Assertions.assertTrue(shampoo instanceof CategoryImpl);

    }

    @Test
    public void constructor_should_throwException_when_nameShorterThanExpected() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> new CategoryImpl("Sh"));
    }

    @Test
    public void addProduct_should_addProductToList() {
        // Arrange
        CategoryImpl category = new CategoryImpl("Cosmetic");
        Product product1 = new ProductImpl("Soap", "Eucerin", 7.50, GenderType.UNISEX);
        Product product2 = new ProductImpl("Sampoo", "Rene", 27.50, GenderType.WOMEN);
        // Act
        category.addProduct(product1);
        category.addProduct(product2);
        // Assert
        Assertions.assertEquals(2, category.getProducts().size());
    }

    @Test
    public void removeProduct_should_removeProductFromList_when_pProductExist() {
        CategoryImpl category = new CategoryImpl("Cosmetic");
        Product product1 = new ProductImpl("Soap", "Eucerin", 7.50, GenderType.UNISEX);
        Product product2 = new ProductImpl("Sampoo", "Rene", 27.50, GenderType.WOMEN);
        category.addProduct(product1);
        category.addProduct(product2);
        category.removeProduct(product1);
        Assertions.assertEquals(1, category.getProducts().size());
    }

    @Test
    public void removeProduct_should_notRemoveProductFromList_when_productNotExist() {
        CategoryImpl category = new CategoryImpl("Cosmetic");
        Product product1 = new ProductImpl("Soap", "Eucerin", 7.50, GenderType.UNISEX);
        category.addProduct(product1);
        category.removeProduct(null);
        Assertions.assertEquals(1, category.getProducts().size());
    }

    @Test
    public void test_getName() {
        CategoryImpl category = new CategoryImpl("Cosmetic");
        Assertions.assertEquals("Cosmetic", category.getName());
    }

}
