package com.telerikacademy.agency.tests.models.vehicles;

import com.telerikacademy.agency.models.vehicles.TrainImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TrainImpl_Tests {
    
    @Test
    public void constructor_should_throw_when_passengerCapacityLessThanMinValue() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TrainImpl(29, 2, 5));
    }
    
    @Test
    public void constructor_should_throw_when_passengerCapacityMoreThanMaxValue() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TrainImpl(151, 2, 5));
    }
    
    @Test
    public void constructor_should_throw_when_cartsLessThanMinValue() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TrainImpl(50, 2, 0));
    }

    @Test
    public void constructor_should_throw_when_cartsMoreThanMaxValue() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TrainImpl(50, 2, 16));
    }
    
}
