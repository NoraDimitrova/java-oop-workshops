package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleBase implements Airplane {
    private boolean hasFreeFood;
    private static final VehicleType type = VehicleType.AIR;


    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(passengerCapacity, pricePerKilometer);
        setHasFreeFood(hasFreeFood);
    }

    public boolean isHasFreeFood() {
        return hasFreeFood;
    }

    private void setHasFreeFood(boolean hasFreeFood) {
        this.hasFreeFood = hasFreeFood;
    }

    @Override
    public String print() {
        return String.format("%s" +
                "Has free food: %s%n", super.print(), this.hasFreeFood);
    }

    @Override
    public boolean hasFreeFood() {
        return false;
    }

    @Override
    public String toString() {
        return print();
    }

    @Override
    public VehicleType getType() {
        return type;
    }
}
