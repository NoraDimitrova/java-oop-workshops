package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

import java.awt.image.BufferStrategy;

public class BusImpl extends VehicleBase implements Bus {
    private static final VehicleType type = VehicleType.LAND;


    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer);
    }

    @Override
    public void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < 10 || passengerCapacity > 50) {
            throw new IllegalArgumentException("A bus cannot have less than 10 passengers or more than 50 passengers.");
        }
        super.setPassengerCapacity(passengerCapacity);
    }

    @Override
    public String print() {
        return String.format("%s", super.print());
    }


    @Override
    public String toString() {
        return print();
    }

    @Override
    public VehicleType getType() {
        return type;
    }
}
