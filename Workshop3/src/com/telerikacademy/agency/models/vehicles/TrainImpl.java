package com.telerikacademy.agency.models.vehicles;


import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train {
    private int carts;
    private static final VehicleType type = VehicleType.LAND;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer);
        setCarts(carts);
    }

    public TrainImpl(int passengerCapacity, double pricePerKilometer, VehicleType type, int carts) {
        super(passengerCapacity, pricePerKilometer, type);
        this.carts = carts;
    }

    @Override
    public int getCarts() {
        return this.carts;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    private void setCarts(int carts) {
        if (carts < 1 || carts > 15) {
            throw new IllegalArgumentException("A train cannot have less than 1 cart or more than 15 carts.");
        }
        this.carts = carts;
    }

    @Override
    public void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < 30 || passengerCapacity > 150) {
            throw new IllegalArgumentException("A train cannot have less than 30 passengers or more than 150 passengers.");
        }
        super.setPassengerCapacity(passengerCapacity);
    }

    @Override
    public String print() {
        return String.format("%s" +
                "Carts amount: %d\n", super.print(), carts);
    }

    @Override
    public String toString() {
        return print();
    }


}
