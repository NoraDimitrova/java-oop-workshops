package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.contracts.Printable;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public class VehicleBase implements Vehicle {
    private static final String TYPE_CANT_BE_NULL = "Type can not be null";
    private int passengerCapacity;
    private double pricePerKilometer;
    private VehicleType type;

    public VehicleBase(int passengerCapacity, double pricePerKilometer) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
    }

    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType type) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        if (type == null) {
            throw new IllegalArgumentException(TYPE_CANT_BE_NULL);
        }
        this.type = type;
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    public VehicleType getType() {
        return type;
    }

    public void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < 1 || passengerCapacity > 800) {
            throw new IllegalArgumentException("A vehicle with less than 1 passengers or more than 800 passengers cannot exist!");
        }
        this.passengerCapacity = passengerCapacity;
    }

    private void setPricePerKilometer(double pricePerKilometer) {
        if (pricePerKilometer < 0.10 || pricePerKilometer > 2.50) {
            throw new IllegalArgumentException("A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!");
        }
        this.pricePerKilometer = pricePerKilometer;
    }

//    @Override
//    public String print() {
//        return String.format("Passenger capacity: %d\n" +
//                "Price per kilometer: %.2f\n" +
//                "Vehicle type: %s\n", passengerCapacity, pricePerKilometer, type);
//    }
//


    @Override
    public String print() {
        return String.format("%s ----%n" +
                        "Passenger capacity: %d%n" +
                        "Price per kilometer: %.2f%n" +
                        "Vehicle type: %s%n",
                this.printClassName(),
                this.getPassengerCapacity(),
                this.getPricePerKilometer(),
                this.getType());
    }

    public String printClassName() {
        String className = getClass().getSimpleName();
        return className.substring(0, className.length() - 4);
    }


}
