package com.telerikacademy.oop.cosmetics.models.cart;

import com.telerikacademy.oop.cosmetics.commands.CommandConstants;
import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private List<Product> productList;

    public ShoppingCart() {
        productList = new ArrayList<>();
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void addProduct(Product product) {
        this.productList.add(product);
    }

    public void removeProduct(Product product) {
        this.productList.remove(product);
    }

    public boolean containsProduct(Product product) {

        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public double totalPrice() {
        //ToDo

       return 0;
    }

//    public String execute(List<String> parameters) {
//        ShoppingCart cart = cosmeticsRepository.getShoppingCart();
//        if (cart.getProductList().size() == 0) {
//            return "No product in shopping cart!";
//        }
//        return String.format(CommandConstants.TOTAL_PRICE_IN_SHOPPING_CART, cart.totalPrice());
//    }

//    public static final String TOTAL_PRICE_IN_SHOPPING_CART = "$%f total price currently in the shopping cart!";

}
