package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.commands.CommandConstants;
import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class Category {

    private String name;
    private List<Product> products;

    public Category(String name) {
        setName(name);
        this.products = new ArrayList<>();
    }

    public void setName(String name) {
        if (name.length() >= 2 && name.length() <= 15) {
            this.name = name;
        }
    }

    public String getName() {
        return name;
    }

    public List<Product> getProducts() {
        return products;
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void addProduct(Product product) {
        products.add(product);

//        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void removeProduct(Product product) {
        products.remove(product);
//        for (Product productToRemove : products) {
//            if (productToRemove.equals(product)) {
//                products.remove(product);
//            }
//        }
    }

    public String print() {

        StringBuilder builder = new StringBuilder();
        if (!products.isEmpty()) {
            builder.append("#Category: " + name);
        } else {
            builder.append("#Category: " + name).append(System.lineSeparator());
            builder.append(" #No product in this category");
        }

        return builder.toString();


    }

}
