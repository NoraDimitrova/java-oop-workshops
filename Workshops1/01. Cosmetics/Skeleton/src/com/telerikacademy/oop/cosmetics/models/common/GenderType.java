package com.telerikacademy.oop.cosmetics.models.common;

public enum GenderType {
    WOMAN,
    UNISEX,
    MEN;
}
