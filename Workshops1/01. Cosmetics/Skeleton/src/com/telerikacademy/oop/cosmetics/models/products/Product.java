package com.telerikacademy.oop.cosmetics.models.products;

import com.telerikacademy.oop.cosmetics.models.common.GenderType;

public class Product {

    private double price;
    private String name;
    private String brand;
    private GenderType gender;

    public Product(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }

    public void setName(String name) {
        if (name.length() >= 3 && name.length() <= 10) {
            this.name = name;
        }
    }

    public void setBrand(String brand) {
        if (brand.length() >= 2 && brand.length() <= 10) {
            this.brand = brand;
        }
    }

    public void setPrice(double price) {
        if (price >= 0) {
            this.price = price;
        }
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }

    public String print() {
        // Format:
        //" #[Name] [Brand]
        // #Price: [Price]
        // #Gender: [Gender]
        // ==="

        StringBuilder builder = new StringBuilder();
        builder.append(" #" + name + " " + brand).append(System.lineSeparator());
        builder.append(" #Price: " + price).append(System.lineSeparator());
        builder.append(" #Gender: " + gender).append(System.lineSeparator());
        builder.append(" ===");

        return builder.toString();
    }
}
