package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public abstract class VehicleBase implements Vehicle {

    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";

    private String make;
    private String model;
    private double price;
    private VehicleType vehicleType;
    private List<Comment> comments;

    public VehicleBase(String make, String model, double price, VehicleType vehicleType) {
        setMake(make);
        setModel(model);
        setPrice(price);
        this.vehicleType = vehicleType;
        this.comments = new ArrayList<>();
    }

    private void setMake(String make) {
        if (make == null) {
            throw new NullPointerException("Make cannot be null");
        }
        if (make.length() < ModelsConstants.MIN_MAKE_LENGTH || make.length() > ModelsConstants.MAX_MAKE_LENGTH) {
            throw new IllegalArgumentException("Make must be between 2 and 15 characters long!");
        }
        this.make = make;
    }

    private void setModel(String model) {
        if (model == null) {
            throw new NullPointerException("Model cannot be null");
        }
        if (model.length() < ModelsConstants.MIN_MODEL_LENGTH || model.length() > ModelsConstants.MAX_MODEL_LENGTH) {
            throw new IllegalArgumentException("Model must be between 1 and 15 characters long!");
        }
        this.model = model;
    }

    private void setPrice(double price) {
        if (price < ModelsConstants.MIN_PRICE || price > ModelsConstants.MAX_PRICE) {
            throw new IllegalArgumentException("Price must be between 0.0 and 1000000.0!");
        }
        this.price = price;
    }

    @Override
    public String getMake() {
        return make;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", "")))
                .append(System.lineSeparator());

        builder.append(String.format("  Make: %s", make)).append(System.lineSeparator());
        builder.append(String.format("  Model: %s", model)).append(System.lineSeparator());
        builder.append(String.format("  Wheels: %s", getWheels())).append(System.lineSeparator());

        builder.append(String.format("  %s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(price)))
                .append(System.lineSeparator());

        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }

    protected abstract String printAdditionalInfo();


    private String printComments() {
        StringBuilder builder = new StringBuilder();

        if (comments.size() <= 0) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());

            for (Comment comment : comments) {
                builder.append(comment.toString());
            }

            builder.append(String.format("%s", COMMENTS_HEADER));
        }

        return builder.toString();
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public int getWheels() {
        return vehicleType.getWheelsFromType();
    }

    @Override
    public VehicleType getType() {
        return vehicleType;
    }

    @Override
    public void removeComment(Comment comment) {
        comments.remove(comment);
    }

    @Override
    public void addComment(Comment comment) {
        comments.add(comment);
    }

}
