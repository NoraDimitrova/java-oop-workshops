package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Truck;

public class TruckImpl extends VehicleBase implements Truck {

    private int weightCapacity;
    private static final VehicleType type = VehicleType.TRUCK;

    public TruckImpl(String make, String model, double price, int capacity) {
        super(make, model, price, type);
        setWeightCapacity(capacity);
    }

    private void setWeightCapacity(int weightCapacity) {
        if (weightCapacity < ModelsConstants.MIN_CAPACITY || weightCapacity > ModelsConstants.MAX_CAPACITY) {
            throw new IllegalArgumentException("Weight capacity must be between 1 and 100!");
        }
        this.weightCapacity = weightCapacity;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  Weight Capacity: %dt", weightCapacity);
    }

    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }

    @Override
    public String toString() {
        return super.toString();
    }


}
