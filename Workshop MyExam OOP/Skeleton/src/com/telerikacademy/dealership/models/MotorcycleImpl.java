package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Motorcycle;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {

    private String category;
    private static final VehicleType type = VehicleType.MOTORCYCLE;

    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price, type);
        setCategory(category);
    }

    private void setCategory(String category) {
        if (category.length() < ModelsConstants.MIN_CATEGORY_LENGTH ||
                category.length() > ModelsConstants.MAX_CATEGORY_LENGTH) {
            throw new IllegalArgumentException("Category must be between 3 and 10 characters long!");
        }
        this.category = category;
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  Category: %s", category);
    }

    @Override
    public String toString() {
        return super.toString();
    }

}
