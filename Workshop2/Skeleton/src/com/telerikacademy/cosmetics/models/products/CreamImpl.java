package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;


public class CreamImpl extends ProductBase implements Cream {
    private ScentType scent;

    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent) {
        super(name, brand, price, gender);
        setScent(scent);
    }

    public CreamImpl(ScentType scent) {
        this.scent = scent;
    }

    private void setScent(ScentType scent) {
        this.scent = scent;
    }

    @Override
    public String getName() {
        return super.getName();

    }

    @Override
    public String getBrand() {
        return super.getBrand();
    }

    @Override
    public double getPrice() {
        return super.getPrice();
    }

    @Override
    public GenderType getGender() {
        return super.getGender();
    }

    @Override
    public ScentType getScent() {
        return this.scent;
    }


    //#Category: Creams
    //#MyCream Nivea
    // #Price: $12.99
    // #Gender: Men
    // #Scent: Lavender
    // ===

    @Override
    public String print() {
        return String.format(super.print() + " #Scent: %s\n" +
                " ===", getScent());
    }


}
