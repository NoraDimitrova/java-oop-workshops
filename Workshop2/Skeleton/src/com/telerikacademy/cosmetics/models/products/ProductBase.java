package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.commands.CommandConstants;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;


public class ProductBase implements Product {
    public static final int MIN_NAME_LENGTH = 3;
    public static final int MAX_NAME_LENGTH = 10;
    public static final int MIN_BRAND_LENGTH = 2;
    public static final int MAX_BRAND_LENGTH = 10;
    private String name;
    private String brand;
    private double price;
    private GenderType gender;

    public ProductBase(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }

    private void setName(String name) {
        if (name == null || name.length() < MIN_NAME_LENGTH || name.length() > MAX_NAME_LENGTH) {
            throw new IllegalArgumentException("The name of product should be between 3 and 10 chars");
        }

        this.name = name;
    }

    private void setBrand(String brand) {
        if (brand == null || brand.length() < MIN_BRAND_LENGTH || brand.length() > MAX_BRAND_LENGTH) {
            throw new IllegalArgumentException("The brand of product should be between 2 and 10 chars");
        }
        this.brand = brand;
    }

    private void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("");
        }
        this.price = price;
    }

    private void setGender(GenderType gender) {
        this.gender = gender;
    }

    ProductBase() {
        this.gender = getGender();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getBrand() {
        return this.brand;
    }

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public GenderType getGender() {
        return this.gender;
    }

    @Override
    public String print() {
        return String.format("#%s %s\n" +
                " #Price: $%.2f\n" +
                " #Gender: %s\n", name, brand, price, gender);

//        StringBuilder builder = new StringBuilder();
//        builder.append(" #" + name + " " + brand).append(System.lineSeparator());
//        builder.append(" #Price: " + price).append(System.lineSeparator());
//        builder.append(" #Gender: " + gender).append(System.lineSeparator());
//        builder.append(" ===");
//        return builder.toString();
    }
}
