package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;


public class ShampooImpl extends ProductBase implements Shampoo {
    private int milliliters;
    private UsageType usage;

    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType usage) {
        super(name, brand, price, gender);
        setMilliliters(milliliters);
        setUsage(usage);
    }

    private void setMilliliters(int milliliters) {
        if (milliliters < 0) {
            throw new IllegalArgumentException("Milliliters are not negative number");
        }
        this.milliliters = milliliters;
    }

    private void setUsage(UsageType usage) {
        this.usage = usage;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getBrand() {
        return super.getBrand();
    }

    @Override
    public double getPrice() {
        return super.getPrice();
    }

    @Override
    public GenderType getGender() {
        return super.getGender();
    }

    @Override
    public int getMilliliters() {
        return this.milliliters;
    }

    @Override
    public UsageType getUsage() {
        return this.usage;
    }

    @Override
    public String print() {
        return String.format(super.print() + " #Milliliters: %s\n" + " #Usage: %s\n" +
                " ===", getMilliliters(), getUsage());
    }
}
