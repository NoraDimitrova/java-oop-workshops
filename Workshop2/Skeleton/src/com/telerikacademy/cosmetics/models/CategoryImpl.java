package com.telerikacademy.cosmetics.models;


import com.telerikacademy.cosmetics.models.contracts.Category;
import com.telerikacademy.cosmetics.models.contracts.Product;
import com.telerikacademy.cosmetics.models.products.ProductBase;


import java.util.ArrayList;
import java.util.List;

public class CategoryImpl implements Category {

    private final String name;
    private final List<Product> products;

    public CategoryImpl(String name) {
        this.name = name;
        this.products = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    public void removeProduct(Product product) {
        products.remove(product);
    }


    public String print() {
        StringBuilder result = new StringBuilder();
        result.append(String.format("#Category: %s\n", name));

        if (products.isEmpty()) {
            result.append(" #No product in this category");
        }

        for (Product product : products) {
            result.append(product.print());
        }

        return result.toString();
    }

}


